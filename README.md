# Install and run
2. Get Python from https://www.python.org/downloads/.

2. (Optional) Create a virtual environment and activate it:

    ```shell
    python3 -m venv env
    ```

   ```shell
   venv/Scripts/activate
   ```
3. Install requirements:

   ```shell
   python -m pip install -r requirements.txt
   ```

4. Create an executable file `dist/app.exe`:

   ```shell
   pyinstaller reflex/main.py --name=app --onefile
   ```
# Run

Results are written to `data/<UUID>.csv` for the single run,as well as appended to `data/all.csv`.

# Changing configuration

The main configuration is in `config.py`. Simple change the values (e.g. number of repeats, sizes, ...). If you want to
change the colours used, make changes to `reflex/colour.py` by adding/removing/changing values:
```python
COLOUR_NAME = "český název", "anglický název"
```
