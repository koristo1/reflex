import random
import tkinter as tk

import tkinter.font as tkFont

from typing import Callable

from reflex.colour import Colour


def create_vote_buttons(root, text_value: Colour, text_colour: Colour, on_button_press: Callable) -> list[tk.Button]:
    buttons = []

    button_font = tkFont.Font(family="Arial", size=20)

    button_frame = tk.Frame(root)
    button_frame.pack()

    buttons.append(tk.Button(button_frame, text=text_value.czech, width=Colour.max_length(), font=button_font,
                             command=lambda: on_button_press(False)))
    buttons.append(tk.Button(button_frame, text=text_colour.czech, width=Colour.max_length(), font=button_font,
                             command=lambda: on_button_press(True)))

    random.shuffle(buttons)

    for button, side in zip(buttons, [tk.LEFT, tk.RIGHT]):
        button.pack(side=side)

    return buttons
