import tkinter as tk
from typing import Optional, Callable


class DismissiblePopup(tk.Toplevel):
    def __init__(self, root=None, title: str = '', message: str = '', on_dismiss: Optional[Callable] = None):
        super().__init__(root)
        self.withdraw()  # Hide the window initially
        self.title(title)

        # Remove window decorations
        self.attributes('-topmost', True)
        self.attributes('-toolwindow', True)
        self.attributes('-alpha', 0.9)
        self.overrideredirect(True)

        # Add message label
        tk.Label(self, text=message).pack(padx=10, pady=10)

        # Add dismiss button
        button_frame = tk.Frame(self)
        button_frame.pack()
        tk.Button(button_frame, text='Start', command=self.destroy).pack(pady=10)

        # Center the window on screen
        self.update_idletasks()
        width = root.winfo_width()
        height = root.winfo_height()
        x = (root.winfo_width() // 2) - (width // 2)
        y = (root.winfo_height() // 2) - (height // 2)
        self.geometry('{}x{}+{}+{}'.format(width, height, 0, 0))

        self.deiconify()  # Show the window

        self._on_dismiss = on_dismiss

    def _dismiss(self):
        self.destroy()  # Close the window
        if self._on_dismiss:
            self._on_dismiss()  # Call the callback function, if provided
