import datetime
import os
import random
import tkinter as tk
import tkinter.font as tkFont
import uuid

import pandas as pd

from reflex.colour import Colour
from reflex.component.vote_buttons import create_vote_buttons
from reflex.config import COLOUR_TEXT_FIELD_HEIGHT, FONT_SIZE, FONT_FAMILY, MASTER_CSV_PATH, WINDOWS_HEIGHT, TEST_SIZE, \
    IGNORE_FIRST, OUTPUT_DIRECTORY
from reflex.event import Event
from reflex.util import read_csv, get_width_to_fit_all

USER_ID = uuid.uuid4()

lap: datetime = None

events = []


def on_button_press(is_correct: bool) -> None:
    global lap

    events.append(Event(start=lap, finish=datetime.datetime.now(), is_correct=is_correct, user_id=USER_ID))

    if len(events) == TEST_SIZE + 1:
        on_exit()
        return

    lap = datetime.datetime.now()

    text_value, text_colour = random.sample(Colour.all(), k=2)

    random.shuffle(buttons)
    buttons[0].config(text=text_value.czech, command=lambda: on_button_press(False))
    buttons[1].config(text=text_colour.czech, command=lambda: on_button_press(True))

    canvas.itemconfig(text_box, text=text_value.czech, fill=text_colour.english)


def on_initial_window_dismiss() -> None:
    global lap
    lap = datetime.datetime.now()


def on_exit() -> None:
    _events = events[1:] if IGNORE_FIRST else events

    if not os.path.isdir(OUTPUT_DIRECTORY):
        os.makedirs(OUTPUT_DIRECTORY)

    current_user_csv_path = os.path.join(OUTPUT_DIRECTORY, f'{USER_ID}.csv')
    current_user_df = pd.DataFrame(_events)

    current_user_df.to_csv(current_user_csv_path, index=False)
    df = read_csv(MASTER_CSV_PATH)
    df = pd.concat([df, current_user_df])
    df.to_csv(MASTER_CSV_PATH, index=False)
    root.destroy()


root = tk.Tk()
root.resizable(False, False)
root.protocol('WM_DELETE_WINDOW', on_exit)

window_width = get_width_to_fit_all(tkFont.Font(family=FONT_FAMILY, size=FONT_SIZE))

root.geometry(f'{window_width}x{WINDOWS_HEIGHT}')

canvas = tk.Canvas(root, width=window_width, height=COLOUR_TEXT_FIELD_HEIGHT)
canvas.pack()

text_value, text_colour = random.sample(Colour.all(), k=2)
text_box = canvas.create_text(window_width / 2, 100, text=text_colour.czech, fill=text_value.english,
                              font=tkFont.Font(family=FONT_FAMILY, size=FONT_SIZE))

buttons = create_vote_buttons(root, text_value, text_colour, on_button_press)

# popup = DismissiblePopup(root, on_dismiss=on_initial_window_dismiss)

root.mainloop()
