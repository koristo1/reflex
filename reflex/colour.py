from enum import Enum


class Colour(Enum):
    RED = 'červená', 'red'
    LIGHT_BLUE = 'světle modrá', 'light blue'
    DARK_BLUE = 'tmavě modrá', 'dark blue'
    YELLOW = 'žlutá', 'yellow'
    LIGHT_GREEN = 'světle zelená', 'light green'
    DARK_GREEN = 'tmavě zelená', 'dark green'
    PURPLE = 'fialová', 'purple'
    BROWN = 'hnědá', 'brown'
    TEAL = 'tyrkysová', 'teal'
    PINK = 'růžová', 'pink'
    ORANGE = 'oranžová', 'orange'
    BLACK = 'černá', 'black'
    WHITE = 'bílá', 'white'

    @staticmethod
    def all() -> list[str]:
        return list(Colour)

    @property
    def czech(self):
        return self.value[0]

    @property
    def english(self):
        return self.value[1]

    @staticmethod
    def longest_colour_name() -> str:
        return max([colour.czech for colour in Colour.all()], key=len)

    @staticmethod
    def max_length():
        return len(Colour.longest_colour_name())

