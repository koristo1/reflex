import datetime
import uuid
from dataclasses import dataclass


@dataclass
class Event:
    start: datetime.datetime
    finish: datetime.datetime
    is_correct: bool
    user_id: uuid.UUID

    @property
    def timedelta(self):
        return self.finish - self.start
