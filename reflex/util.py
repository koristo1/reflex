import math
import os

import pandas as pd

from reflex.colour import Colour


def read_csv(path: str) -> pd.DataFrame:
    directory = os.path.dirname(path)
    if not os.path.isdir(directory):
        os.makedirs(directory, exist_ok=True)

    if os.path.isfile(path) and not is_empty(path):
        df = pd.read_csv(path, index_col=False)
    else:
        df = pd.DataFrame()

    return df


def is_empty(path: str):
    return os.path.getsize(path) == 0


def get_width_to_fit_all(font) -> int:
    exact_width = font.measure('x' * Colour.max_length())
    return math.ceil(exact_width * 1.1)
